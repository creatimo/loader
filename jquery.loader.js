/*------------------------/
 *	Asynchronous content Loader
 *	Author: Adam Javurek
 /*------------------------*/

$.fn.loader = function(param1, param2, param3) {

	/* Default settings */
	var settings = $.fn.loader.defaults;

	if (param1 === undefined || typeof param1 === 'object') {

		/* Extend default settings */
		settings = $.extend( {}, $.fn.loader.defaults, param1 );

		/* Find targets */
		var _toLoad =
				this
				.find('*')
				.andSelf()
				.filter(function(){
					return $(this).attr('data-'+settings.attribute) !== undefined;
				});

		/* Process */
		return _toLoad.each(function(){

			var $this    = $(this),
				dataLoad = $this.attr('data-'+settings.attribute),
				dataAttr = $this.attr('data-'+settings.attribute+'-attr');
			
			if (dataAttr !== undefined)
				dataAttr = JSON.stringify( JSON.parse( dataAttr.replace(/(\{|,)\s*(.+?)\s*:/g, '$1 "$2":') ) );

			$.ajax({
				url:       settings.source,
				type:      settings.method,
				data:      {dataload: dataLoad, dataattr: dataAttr},
				dataType: "html",
				timeout:   settings.timeout,
				beforeSend: function(){

					/* Action on start ajax */
					$.fn.loader.defaults.onBefore($this);

				},
				success: function(result){

					$this
						.html(result)
						.removeAttr('data-'+settings.attribute)
						.removeAttr('data-'+settings.attribute+'-attr');
					
					/* Call loader on actually loaded element */
					$this.loader();

				},
				error: function(err){

					/* Stop on 404 */
					if( err.status == 404 ) {
						$this.html('no source file');
					}

					/* Try again on timeout in specified interval */
					if( err.statusText == 'timeout' ) {
						$this.html('zkusim za chvili');
						var againLoader = setTimeout(function(){
							$this.loader();
						}, settings.delay);
					}

				},
				complete: function(){

					/* Action on complete ajax */
					$.fn.loader.defaults.onComplete($this);

				}
			});

		});

	} else {

		/* Extend default settings */
		settings = $.extend( {}, $.fn.loader.defaults, param3 );

		alert( $.fn.loader.defaults.source );

		// assign Param1 to element
		if (param1 !== undefined) {
			this.attr('data-'+settings.attribute, param1);
		}

		// assign Param2 to element
		if (param2 !== undefined) {
			this.attr('data-'+settings.attribute+'-attr', param2);
		}

		// run Loader
		this.loader();

	}

}


/* Default settings */
$.fn.loader.defaults = {
	attribute: "load",			// name of data attribute with content identify
	method:    "POST",			// send method
	timeout:   5000,			// timeout for ajax
	delay:     5000				// delay before trying on error
};
$.fn.loader.defaults.source = "loader.php"; // response controlling rpc


/* Ajax start element action */
$.fn.loader.defaults.onBefore = function(elm){
	elm.html('loading');
};


/* Ajax start element action */
$.fn.loader.defaults.onComplete = function(elm){
	//console.log('complete');
};


/* Run loader */
$(function(){
	$(document).loader();
});
